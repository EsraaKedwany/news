<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\News;

class NewsTransformer extends TransformerAbstract
{
    public function transform(News $news)
    {
        return [
            'title' => $news->title,
            'body' => $news->body,
            'user_id' => $news->user_id,
        ];
    }
}