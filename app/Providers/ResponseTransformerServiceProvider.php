<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ResponseTransformerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // $app['Dingo\Api\Transformer\Factory']->setAdapter(function ($app) {
        //     $fractal = new League\Fractal\Manager;
        
        //     $fractal->setSerializer(new League\Fractal\Serializer\JsonApiSerializer);
        
        //     return new Dingo\Api\Transformer\Adapter\Fractal($fractal);
        // });
        $this->app['Dingo\Api\Transformer\Factory']->setAdapter(function ($app) {
            return new Dingo\Api\Transformer\Adapter\Fractal(new League\Fractal\Manager, 'include', ',');
       });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
