<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ThrottlingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $app['Dingo\Api\Http\RateLimit\Handler']->extend(function ($app) {
            return new Dingo\Api\Http\RateLimit\Throttle\Authenticated;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
