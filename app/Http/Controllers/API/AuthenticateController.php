<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use JWTFactory;
use Validator;
use Response;

/**
 *
 * Authentication resource representation.
 * 
 * @Resource("Authentication")
 */
class AuthenticateController extends Controller
{
    /**
     * Display the login.
     *
     * Get a JSON representation of user's token
     * 
     * @Post("/login")
     * @Versions({"v1"})
     * @Request({"email": "ali@gmail.com", "password": "12345678"}, headers={"Content-Type": "application/x-www-form-urlencoded"})
     * @Response(200, body={"token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N..."})
     */
    public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $user = User::where('email','=',$request->get('email'))->first();
        $user->token = $token;
        $user->save();

        // all good so return the token
        return response()->json(compact('token'));
    }

    /**
     * Display the registeration.
     *
     * Get a JSON representation of user's token
     * 
     * @Post("/register")
     * @Versions({"v1"})
     * @Request({"name": "ali", "email": "ali@gmail.com", "password": "12345678"})
     * @Response(200, body={"token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N..."})
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required',
            'password'=> 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ]);
        $user = User::first();
        $token = JWTAuth::fromUser($user);
        
        return Response::json(compact('token'));
    }
}
