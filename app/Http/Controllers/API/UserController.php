<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Http\Controllers\BaseController;
use App\Transformers\UserTransformer;

/**
 *
 * User resource representation.
 * 
 * @Resource("Users")
 */
class UserController extends BaseController
{
    use Helpers;

    public function __construct()
    {
        $this->middleware('api.auth');

    }

    /**
     * Display the specified user.
     *
     * Get a JSON representation of registered user
     * 
     * @Get("/user/{id}")
     * @Versions({"v1"})
     * @Request( body={"Token": "Bearer Token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N..."})
     * @Response(200, body={"id": 1, "name": "ali", "email": "ali@gmail.com"} )
     * Note: Each user can show his data only.
     */
    public function show($id)
    {
        // dd($id);
        // if(app('Dingo\Api\Auth\Auth')->user()===$id)
        //     dd("true");
        $user = User::findOrFail($id);

        if (app('Dingo\Api\Auth\Auth')->user()->id != $id) {
            return $this->response->error('cannot access.', 401);
        }

        return $this->response->item($user, new UserTransformer);
    }

}
