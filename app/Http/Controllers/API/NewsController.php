<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\News;
use App\Http\Controllers\BaseController;
use App\Transformers\NewsTransformer;
use Illuminate\Support\Facades\Validator;

/**
 * News resource representation.
 * 
 * @Resource("News")
 */
class NewsController extends BaseController
{
    /**
     * Display a list of news.
     *
     * Get a JSON representation all news
     * 
     * @Get("/news")
     * @Versions({"v1"})
     * @Request( {"token": "Bearer Token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N..."})
     * @Response(200, body={"title": "my first title", "body": "my first news", "user_id": 1})
     */
    public function index()
    {
        $news= News::all();
        return $this->response->collection($news, new NewsTransformer);
    }

     /**
     * Store a newly created news in storage.
     * 
     * @param  \Illuminate\Http\Request  $request
     * 
     * @Post("/news")
     * @Versions({"v1"})
     * @Request(body={"title": "my first title", "body": "my first news", "user_id": 1,"token": "Bearer Token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N..."})
     * @Response(201)
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required| min:3| max:150',
            'body' => 'required| min:7',
        ]);
        if ($validator->fails()) 
        {
            return $this->response->error('cannot create.', 422, $validator->errors());
        }
        else
        {
            $news=News::create([
                'title'=>$request->title,
                'body'=>$request->body,
                'user_id'=>app('Dingo\Api\Auth\Auth')->user()->id
            ]);
            return $this->response->created();
        }        
    }

    /**
     * Display the specified news.
     * 
     * Get a JSON representation specified news
     * 
     * @param  int  $id
     * 
     * @Get("/news/{id}")
     * @Versions({"v1"})
     * @Request(body={"token": "Bearer Token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N..."})
     * @Response(200, body={"title": "my first title", "body": "my first news", "user_id": 1})
     */
    public function show($id)
    {
        $news=News::findOrFail($id);
        return $this->response->item($news, new NewsTransformer);
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  int  $id, \Illuminate\Http\Request  $request
     * 
     * @Put("/news/{id}/edit")
     * @Versions({"v1"})
     * @Request(body={"title": "updated title", "body": "updated news", "token": "Bearer Token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N..."})
     * @Response(200, body={"title": "updated title", "body": "updated news", "user_id": 1} )
     * Note: news owner only can update it.
     */
    public function update(Request $request, $id)
    {
        $news=News::findOrFail($id);
        if (app('Dingo\Api\Auth\Auth')->user()->id == $news->user_id) 
        {
                $validator = Validator::make($request->all(), [
                'title' => 'required| min:3| max:150',
                'body' => 'required| min:7',
            ]);
            if ($validator->fails()) 
            {
                return $this->response->error('cannot update.', 422, $validator->errors());
            }
            else
            {
                $news->update([
                    'title' => $request->title,
                    'body' => $request->body,
                ]);
                return $this->response->item($news, new NewsTransformer);
            } 
        }
        else{
            return $this->response->error('you are not the writer.', 401);
        }
        
    }

     /**
     * Remove the specified news from storage.
     *
     * @param  int  $id
     * 
     * @Delete("/news/{id}")
     * @Versions({"v1"})
     * @Request(body={"token": "Bearer Token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N..."})
     * @Response(204)
     * Note: news owner only can delete it.
     */
    public function destroy($id)
    {
        $news=News::findOrFail($id);
        if (app('Dingo\Api\Auth\Auth')->user()->id == $news->user_id) 
        {
            $news->delete();
        return response()->json(null, 204);
        }
        else
        {
            return $this->response->error('you are not the writer.', 401);
        }
        

    }
}
