FORMAT: 1A

# NewsAPI

# News
News resource representation.

## Display a list of news. [GET /news]
Get a JSON representation all news

+ Request (application/json)
    + Body

            {
                "token": "Bearer Token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N..."
            }

+ Response 200 (application/json)
    + Body

            {
                "title": "my first title",
                "body": "my first news",
                "user_id": 1
            }

## Store a newly created news in storage. [POST /news]


+ Request (application/json)
    + Body

            {
                "title": "my first title",
                "body": "my first news",
                "user_id": 1,
                "token": "Bearer Token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N..."
            }

+ Response 201 (application/json)

## Display the specified news. [GET /news/{id}]
Get a JSON representation specified news

+ Request (application/json)
    + Body

            {
                "token": "Bearer Token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N..."
            }

+ Response 200 (application/json)
    + Body

            {
                "title": "my first title",
                "body": "my first news",
                "user_id": 1
            }

## Update the specified resource in storage. [PUT /news/{id}/edit]


+ Request (application/json)
    + Body

            {
                "title": "updated title",
                "body": "updated news",
                "token": "Bearer Token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N..."
            }

+ Response 200 (application/json)
    + Body

            {
                "title": "updated title",
                "body": "updated news",
                "user_id": 1
            }

## Remove the specified news from storage. [DELETE /news/{id}]


+ Request (application/json)
    + Body

            {
                "token": "Bearer Token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N..."
            }

+ Response 204 (application/json)

# Users
User resource representation.

## Display the specified user. [GET /user/{id}]
Get a JSON representation of registered user

+ Request (application/json)
    + Body

            {
                "Token": "Bearer Token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N..."
            }

+ Response 200 (application/json)
    + Body

            {
                "id": 1,
                "name": "ali",
                "email": "ali@gmail.com"
            }

# Authentication
Authentication resource representation.

## Display the login. [POST /login]
Get a JSON representation of user's token

+ Request (application/json)
    + Headers

            Content-Type: application/x-www-form-urlencoded
    + Body

            {
                "email": "ali@gmail.com",
                "password": "12345678"
            }

+ Response 200 (application/json)
    + Body

            {
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N..."
            }

## Display the registeration. [POST /register]
Get a JSON representation of user's token

+ Request (application/json)
    + Body

            {
                "name": "ali",
                "email": "ali@gmail.com",
                "password": "12345678"
            }

+ Response 200 (application/json)
    + Body

            {
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N..."
            }