<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['middleware' => 'api.auth'], function ($api) {
        // News routes
        $api->get('news', 'App\Http\Controllers\API\NewsController@index');
        $api->get('news/{id}', 'App\Http\Controllers\API\NewsController@show');
        $api->post('news', 'App\Http\Controllers\API\NewsController@store');
        $api->put('news/{id}/edit', 'App\Http\Controllers\API\NewsController@update');
        $api->delete('news/{id}', 'App\Http\Controllers\API\NewsController@destroy');
        // User routes
        $api->get('user/{id}', 'App\Http\Controllers\API\UserController@show');
    });

    // Auth routes
    $api->post('register', 'App\Http\Controllers\API\AuthenticateController@register');
    $api->post('login', 'App\Http\Controllers\API\AuthenticateController@authenticate');
});

    