This README would normally document whatever steps are necessary to get the application up and running.

-Things that covered in project:
        - php: 7.1.3
        - laravel: 5.8

-Configuration:
  create .env file and generate app_key:
          php artisan key:generate
          APP_KEY=×××××××××××××××××××××××××××
  and add database name, username, password:
          DB_DATABASE=DATA_BASE_NAME
          DB_USERNAME=DATA_BASE_USER_NAME
          DB_PASSWORD=DATA_BASE_PASSWORD
          
-To run database migration:
        php artisan migrate
        
-To run Application:
        php artisan serve
        
-Packages:
        dingo/api: ^2
        jwt-auth: 0.5.*
        
-API documentation in doc.md file

